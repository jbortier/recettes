# Chili sin carne
## Ingrédients
Pour 6 personnes
* 50 g de protéines de soja texturées (petits morceaux)
* 1 cube de bouillon de légumes
* 1 cuillère à café de miso d'orge brun
* 1,5 cuillères à café d'ail moulu
* 3,5 cuillères à café de cumin moulu
* 3 cuillères à soupe d'huile d'olive
* 1 oignon
* 3 gousses d'ail
* 1 carotte
* 3 cuillères à café de coriandre moulue
* 1 cuillère à café d'origan séché
* 1,5 cuillères à café de paprika
* 1 pointe de couteau de piment de Cayenne
* 2 cuillères à soupe de tamari
* 800g de purée de tomates
* 500g de haricots rouges cuits
* 100g de maïs doux
## Préparation
1. Dans un bol, mélanger les protéines de soja avec le bouillon, le miso, ½ cuillère à café d'ail moulu, ½ cuillère à café de cumin. 
1. Couvrir d'eau bouillante et laisser se réhydrater pendant 15 à 30 minutes.
1. Émincer l'oignon et l'ail.
1. Dans une sauteuse, faire revenir à feu vif l'oignon et l'ail.
1. Couper la carotte en petits dés.
1. Ajouter la carotte, le reste des épices, les protéines de soja avec leur jus et le tamari. 
1. Bien mélanger et cuire quelques minutes.
1. Ajouter la purée de tomates.
1. Bien mélanger et cuire quelques minutes à nouveau.
1. Rincer et égouter les haricots rouges et le mais.
1. Ajouter les haricots et le mais.
1. Bien mélanger et cuire à feu doux pendant 20 minutes.

## Source
[Vegan de Marie Lafôret](https://www.laplage.fr/catalogue/vegan2/)