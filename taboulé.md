# Taboulé
## Ingrédients
Pour 4 personnes
* 1 tasse de semoule
* 1 tasse d'eau froide
* 2 cuillères à soupe de menthe séchée
* ½ cuillères à café de sel
* 50g de raisins secs
* 1 petit poivron (environ 250g)
* 300g de tomates
* 1 citron
* 3 cuillères à soupe d'huile d'olive
## Préparation
1. Dans un saladier, mettre la semoule, la menthe et le sel puis mélanger.
1. Ajouter l'eau et laisser gonfler pendant 30 minutes en mélangeant de temps en temps pour aérer la semoule.
1. Couper en petits dés le poivron et les tomates.
1. Ajouter les raisins secs, le poivron, les tomates, le jus du citron, l'huile d'olive et mélanger.
1. Laisser reposer au frais au moins 2 heures.