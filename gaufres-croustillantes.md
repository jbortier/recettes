# Gaufres croustillantes
## Ingrédients
Pour 8 gaufres
* 300g de farine
* 5g de levure chimique (½ sachet)
* 2 cuillères à soupe de fécule de mais
* 1 cuillère à soupe de purée d'amande
* 300g de lait végétal
* 3 cuillères à soupe d'huile d'olive
* ½ cuillère à café de sel (pour un goût d'œuf, utiliser du sel kala namak)
* 20g de sucre en poudre
* 100g de margarine
* (Optionel) 1 cuillère à soupe d'arôme (zeste, vanille, alcool, etc)
## Préparation
1. Mélanger tous les ingrédients secs dans un saladier et creuser un puits.
1. Ajouter la purée d'amande.
1. Ajouter le lait petit à petit en mélangeant.
1. Mélanger jusqu'à obtenir une pâte bien lisse.
1. Faire fondre la margarine
1. Ajouter la margarine à la pâte et mélanger
1. (Optionel) Ajouter l'arôme et mélanger
1. Laisser la pâte reposer une heure à température ambiante, sous un torchon.
1. Cuire environ 5 minutes dans un gaufrier bien chaud. 
## Source
[Déliacious](https://deliacious.com/2020/05/gaufres-vegan.html)