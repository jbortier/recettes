# Pesto crémeux
## Ingrédients
Pour 4 personnes
* 30 g de feuilles de basilic frais
* 70 g de noix de cajou blanches
* 75 ml d'huile d'olive
* ½ cuillère à café de sel
* 1 cuillère à soupe de jus de citron
* 1 gousse d'ail

## Préparation
1. Laver les feuilles de basilic et bien les sécher
2. Verser tous les ingrédients dans un mixer / blender / bocal pour un mixer plongeant
3. Mixer jusqu'à obtenir un pesto crémeux
4. Mettre dans un bocal, conserver au frais et consommer dans les 72 heures

## Source
[Vegan débutant de Marie Laforêt](https://www.laplage.fr/catalogue/vegan-pour-debutant-marie-laforet/)