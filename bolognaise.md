# Bolognaise
## Ingrédients
Pour 6 personnes
* 400g de tofu ferme
* 1 carotte
* 1 oignon
* 1 branche de celeri
* 2 cuillères à soupe de tamari
* ½ tasse de vin rouge
* 1 cube de bouillon de légumes
* 1 boite de concentré de tomates
* 680g de purée de tomate
## Préparation
1. Couper la carotte en petits dés et émincer l'oignon et le celeri.
1. Dans une sauteuse, faire revenir la carotte, l'oignon et le celeri à feu moyen.
1. Émietter le tofu
1. Quand les légumes sont fondants, ajouter le tofu, le tamari, le vin rouge et mélanger. Laisser cuire jusqu'à évaporation.
1. Dans un bol, mélanger le bouillon et le concentré de tomates avec de l'eau bouillante.
1. Verser le bol dans la sauteuse, mélanger et laisser cuire 5 minutes.
1. Ajouter la purée de tomates et laisser cuire 20 minutes à feu doux à couvert.