# Houmous
## Ingrédients
Pour 1 grand bocal
* 1 grande conserve de pois chiches égouttés (poids net 650g, 450g égoutté)
* 3 cuillères à soupe de purée de sésame (Tahin)
* 3 cuillères à soupe de jus de citron
* 2 cuillères à soupe d'huile d'olive
* ½ cuillère à café de paprika
* ¾ cuillère à café de coriandre en poudre
* 1 cuillère à café de cumin en poudre
* 1 gousse d'ail épluchée et dégermée
* Sel au goût
* 2 tours de moulin de poivre
## Préparation
1. Ajouter tous les ingrédients dans un grand bocal (Le Parfait 1L).
1. Ajouter de l'eau à niveau.
1. Mixer au mixer plongeant jusqu'à la texture voulue, rectifier l'assaisonnement et rajouter de l'eau si c'est trop sec. 